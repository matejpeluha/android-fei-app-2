package com.example.peluhafeilastapplication2.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.peluhafeilastapplication2.model.database.PubCounterDataModel
import com.example.peluhafeilastapplication2.model.database.PubDetailDataModel

@Dao
interface PubCounterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pubCounterDataModels: Array<PubCounterDataModel>)

    @Query("SELECT * FROM pub_counter")
    fun getAll(): LiveData<Array<PubCounterDataModel>>

    @Query("SELECT * FROM pub_counter WHERE id=:id LIMIT 1")
    suspend fun getOne(id: Long): PubCounterDataModel

    @Query("DELETE FROM pub_counter")
    suspend fun deleteAll()
}