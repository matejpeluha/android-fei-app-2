package com.example.peluhafeilastapplication2.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.peluhafeilastapplication2.model.database.*
import com.example.peluhafeilastapplication2.viewmodels.PubDetailViewModel

@Database(entities = [NearPubDataModel::class, FriendDataModel::class, UserLocationDataModel::class, PubDetailDataModel::class, PubCounterDataModel::class], version = 16, exportSchema = false)
abstract class PubRoomDatabase : RoomDatabase() {
    abstract fun nearPubDao(): NearPubDao
    abstract fun friendDao(): FriendDao
    abstract fun userLocationDao(): UserLocationDao
    abstract fun pubDetailDao(): PubDetailDao
    abstract fun pubCounterDao(): PubCounterDao
    companion object {
        @Volatile
        private var INSTANCE: PubRoomDatabase? = null
        fun getDatabase(context: Context): PubRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PubRoomDatabase::class.java,
                    "pub_database")
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}