package com.example.peluhafeilastapplication2.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.peluhafeilastapplication2.model.database.FriendDataModel

@Dao
interface FriendDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(friendDataModel: FriendDataModel): Long

    @Update
    suspend fun update(friendDataModel: FriendDataModel): Int

    @Delete
    suspend fun delete(friendDataModel: FriendDataModel)

    @Query("SELECT * from friend WHERE user_id=:userId AND friend_id!=:userId")
    fun getAllFriends(userId: Int): LiveData<Array<FriendDataModel>>
}