package com.example.peluhafeilastapplication2.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.peluhafeilastapplication2.model.database.UserLocationDataModel

@Dao
interface UserLocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userLocation: UserLocationDataModel): Long

    @Delete
    suspend fun delete(userLocation: UserLocationDataModel)

    @Query("SELECT * FROM user_location WHERE user_id=:userId")
    fun getLocation(userId: Int): LiveData<UserLocationDataModel>
}