package com.example.peluhafeilastapplication2.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel
import com.example.peluhafeilastapplication2.model.database.PubDetailDataModel

@Dao
interface PubDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(pubDetailDataModel: PubDetailDataModel): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pubDetailDataModels: Array<PubDetailDataModel>)

    @Delete
    suspend fun delete(pubDetailDataModel: PubDetailDataModel)

    @Query("DELETE FROM pub_detail")
    suspend fun deleteAll()

    @Query("SELECT * from pub_detail")
    fun getAllPubs(): LiveData<Array<PubDetailDataModel>>

    @Query("SELECT * FROM pub_detail WHERE id=:id")
    suspend fun getPub(id: Long): PubDetailDataModel
}