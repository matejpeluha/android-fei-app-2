package com.example.peluhafeilastapplication2.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel

@Dao
interface NearPubDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(nearPubDataModel: NearPubDataModel): Long

    @Delete
    suspend fun delete(nearPubDataModel: NearPubDataModel)

    @Query("DELETE FROM near_pub WHERE user_id=:userId")
    suspend fun deleteForUser(userId: Int)

    @Query("SELECT * from near_pub WHERE user_id=:userId ORDER BY distance ASC")
    fun getAllPubsForUser(userId: Int): LiveData<Array<NearPubDataModel>>

    @Query("SELECT * FROM near_pub WHERE id=:id")
    fun getPub(id: Long): LiveData<NearPubDataModel>

    @Query("SELECT * from near_pub")
    fun getAllPubs(): LiveData<Array<NearPubDataModel>>

    @Query("SELECT * from near_pub ORDER BY name ASC")
    fun getAllPubsAsc(): LiveData<Array<NearPubDataModel>>

    @Query("SELECT * from near_pub ORDER BY name DESC")
    fun getAllPubsDesc(): LiveData<Array<NearPubDataModel>>
}