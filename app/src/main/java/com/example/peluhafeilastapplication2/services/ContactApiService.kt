package com.example.peluhafeilastapplication2.services

import com.example.peluhafeilastapplication2.Constants
import com.example.peluhafeilastapplication2.model.apiRequest.ContactInfo
import com.example.peluhafeilastapplication2.model.apiResponse.Friend
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


interface ContactApiService {
    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @POST("message.php")
    suspend fun addFriend(@Header("x-user") userId: Int, @Header("authorization") bearerToken: String, @Body contactInfo: ContactInfo)

    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @POST("delete.php")
    suspend fun removeFriend(@Header("x-user") userId: Int, @Header("authorization") bearerToken: String, @Body contactInfo: ContactInfo)

    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @GET("list.php")
    suspend fun getFriends(@Header("x-user") userId: Int, @Header("authorization") bearerToken: String): Array<Friend>
}

object ContactApi {
    private const val BASE_URL = Constants.BASE_URL + "contact/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    val retrofitService: ContactApiService by lazy {
        retrofit.create(ContactApiService::class.java)
    }
}