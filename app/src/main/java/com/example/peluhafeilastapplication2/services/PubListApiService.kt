package com.example.peluhafeilastapplication2.services

import com.example.peluhafeilastapplication2.model.apiResponse.AllPubs
import com.example.peluhafeilastapplication2.model.apiRequest.FoundPubList
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


interface PubListApiService {
    @Headers("api-key: KHUu1Fo8042UwzczKz9nNeuVOsg2T4ClIfhndD2Su0G0LHHCBf0LnUF05L231J0M")
    @POST("find")
    suspend fun getPubList(@Body foundPubListApiBody: FoundPubList): AllPubs
}

object PubListApi {
    private const val BASE_URL = "https://data.mongodb-api.com/app/data-fswjp/endpoint/data/v1/action/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    val retrofitService: PubListApiService by lazy {
        retrofit.create(PubListApiService::class.java)
    }
}


