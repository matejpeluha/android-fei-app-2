package com.example.peluhafeilastapplication2.services

import com.example.peluhafeilastapplication2.Constants
import com.example.peluhafeilastapplication2.model.apiRequest.ContactInfo
import com.example.peluhafeilastapplication2.model.apiRequest.PinnedPub
import com.example.peluhafeilastapplication2.model.apiRequest.UserLogin
import com.example.peluhafeilastapplication2.model.apiResponse.PubDetailResponse
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

interface PubApiService {
    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @POST("message.php")
    suspend fun locateInPub(@Header("x-user") userId: Int, @Header("authorization") bearerToken: String, @Body pinnedPub: PinnedPub)

    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @GET("list.php")
    suspend fun getLocatedPubs(@Header("x-user") userId: Int, @Header("authorization") bearerToken: String): Array<PubDetailResponse>
}

object PubApi {
    private const val BASE_URL = Constants.BASE_URL + "bar/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    val retrofitService: PubApiService by lazy {
        retrofit.create(PubApiService::class.java)
    }
}