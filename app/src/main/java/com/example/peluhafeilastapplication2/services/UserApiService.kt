package com.example.peluhafeilastapplication2.services


import com.example.peluhafeilastapplication2.Constants
import com.example.peluhafeilastapplication2.model.apiRequest.UserLogin
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST


interface UserApiService {
    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @POST("create.php")
    suspend fun registerUser(@Body user: UserLogin): UserCredentials

    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @POST("login.php")
    suspend fun loginUser(@Body user: UserLogin): UserCredentials

    @Headers("x-apikey:${Constants.API_KEY}", "Accept: application/json", "Cache-Control: no-cache", "Content-Type: application/json")
    @POST("refresh.php")
    suspend fun refreshCredentials(@Header("x-user") userId: Int, @Body oldCredentials: UserCredentials): UserCredentials
}

object UserApi {
    private const val BASE_URL = Constants.BASE_URL + "user/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    val retrofitService: UserApiService by lazy {
        retrofit.create(UserApiService::class.java)
    }
}