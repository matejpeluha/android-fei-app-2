package com.example.peluhafeilastapplication2.services

import com.example.peluhafeilastapplication2.model.apiRequest.Coordinates
import com.example.peluhafeilastapplication2.model.apiRequest.FoundPubList
import com.example.peluhafeilastapplication2.model.apiResponse.AllPubs
import com.example.peluhafeilastapplication2.model.database.UserLocationDataModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface OverpassApiService {
    @GET("interpreter")
    suspend fun getResponse(@Query("data") data: String): AllPubs
}

object OverpassApi {
    private const val BASE_URL = "https://overpass-api.de/api/"

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    val retrofitService: OverpassApiService by lazy {
        retrofit.create(OverpassApiService::class.java)
    }

    fun getAllPubsQuery(userLocation: UserLocationDataModel): String {
        return "[out:json];node(around:250,${userLocation.lat}, ${userLocation.lon});(node(around:250)[\"amenity\"~\"^pub\$|^bar\$|^restaurant\$|^cafe\$|^fast_food\$|^stripclub\$|^nightclub\$\"];);out body;>;out skel;"
    }

    fun getPubQuery(pubId: Long): String {
        return "[out:json];node(${pubId});out body;>;out skel;"
    }
}