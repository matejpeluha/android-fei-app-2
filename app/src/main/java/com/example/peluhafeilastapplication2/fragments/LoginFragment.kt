package com.example.peluhafeilastapplication2.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.databinding.FragmentLoginBinding
import com.example.peluhafeilastapplication2.model.apiRequest.UserLogin
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModelFactory


class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    private val viewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)

        binding.loginButton.setOnClickListener { handleLogin() }
        binding.registrationButton.setOnClickListener { handleGoToRegistration() }
        observeViewModel()

        viewModel.tryAutoLogin(requireContext())

        return binding.root
    }

    private fun handleLogin() {
        val user = getUser()
        val clientError = viewModel.clientValidation(user)
        if (clientError) return
        if (Connection.isOnline(requireContext())) {
            viewModel.loginUser(user, requireContext())
        } else {
            AlertDialog.Builder(context).setTitle("Info").setMessage("NO Internet connection").create().show()
        }
    }

    private fun getUser(): UserLogin {
        return UserLogin(
            binding.usernameInput.text.toString(),
            viewModel.hashPassword(binding.passwordInput.text.toString()),
            null
        )
    }

    private fun handleGoToRegistration() {
        val action = LoginFragmentDirections.actionLoginFragmentToRegistrationFragment()
        findNavController().navigate(action)
    }

    private fun observeViewModel(){
        viewModel.usernameErrorVisibility.observe(viewLifecycleOwner) {
                value -> binding.usernameError.visibility = value
        }
        viewModel.passwordErrorVisibility.observe(viewLifecycleOwner) {
                value -> binding.passwordError.visibility = value
        }
        viewModel.serverErrorVisibility.observe(viewLifecycleOwner) {
                value -> binding.serverError.visibility = value
        }
        viewModel.serverAccept.observe(viewLifecycleOwner) {
                isAccepted -> goToApp(isAccepted)
        }
        viewModel.isLoading.observe(viewLifecycleOwner) {
            value -> binding.animationView.visibility = value
        }
    }

    private fun goToApp(isAccepted: Boolean) {
        if (isAccepted) {
            val action = LoginFragmentDirections.actionLoginFragmentToMainFragment()
            findNavController().navigate(action)
//            AlertDialog.Builder(context).setTitle("Info").setMessage("You are logged").create().show()
        }
    }
}