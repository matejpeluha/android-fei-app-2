package com.example.peluhafeilastapplication2.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.databinding.FragmentRegistrationBinding
import com.example.peluhafeilastapplication2.model.apiRequest.UserLogin
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModelFactory

class RegistrationFragment : Fragment() {
    private lateinit var binding: FragmentRegistrationBinding
    private val viewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegistrationBinding.inflate(inflater, container, false)

        binding.registrationButton.setOnClickListener { handleRegistration() }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    private fun handleRegistration() {
        val user = getUser()
        val clientError = viewModel.clientValidation(user)
        if (clientError) return
        if (Connection.isOnline(requireContext())) {
            viewModel.registerUser(user)
        } else {
            AlertDialog.Builder(context).setTitle("Info").setMessage("NO Internet connection").create().show()
        }
    }

    private fun getUser(): UserLogin {
        return UserLogin(
            binding.usernameInput.text.toString(),
            viewModel.hashPassword(binding.passwordInput.text.toString()),
            viewModel.hashPassword(binding.secondPasswordInput.text.toString())
        )
    }

    private fun observeViewModel(){
        viewModel.usernameErrorVisibility.observe(viewLifecycleOwner) {
            value -> binding.usernameError.visibility = value
        }
        viewModel.passwordErrorVisibility.observe(viewLifecycleOwner) {
            value -> binding.passwordError.visibility = value
        }
        viewModel.serverErrorVisibility.observe(viewLifecycleOwner) {
            value -> binding.serverError.visibility = value
        }
        viewModel.secondPasswordErrorVisibility.observe(viewLifecycleOwner) {
            value -> binding.secondPasswordError.visibility = value
        }
        viewModel.serverAccept.observe(viewLifecycleOwner) {
            isAccepted -> goToLogin(isAccepted)
        }
        viewModel.isLoading.observe(viewLifecycleOwner) {
                value -> binding.animationView.visibility = value
        }
    }

    private fun goToLogin(isAccepted: Boolean) {
        if (isAccepted) {
            val action = RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
            findNavController().navigate(action)
            viewModel.reloadServerAcceptRegistration()
            AlertDialog.Builder(context).setTitle("Info").setMessage("You are registered").create().show()
        }
    }
}