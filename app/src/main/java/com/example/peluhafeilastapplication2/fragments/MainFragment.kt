package com.example.peluhafeilastapplication2.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.peluhafeilastapplication2.databinding.FragmentMainBinding
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModelFactory

class MainFragment : Fragment() {
    lateinit var binding: FragmentMainBinding
    private val credentialsViewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMainBinding.inflate(inflater, container, false)

        binding.addFriendButton.setOnClickListener { handleAddFriend() }
        binding.logoutButton.setOnClickListener { handleLogOut() }
        binding.friendListButton.setOnClickListener { handleFriendList() }
        binding.pinPubButton.setOnClickListener { handleSetPub() }
        binding.showPubsButton.setOnClickListener { handleAllPubsList() }

        return binding.root
    }

    private fun handleAllPubsList() {
        val action = MainFragmentDirections.actionMainFragmentToAllPubsFragment()
        findNavController().navigate(action)
    }

    private fun handleAddFriend() {
        val action = MainFragmentDirections.actionMainFragmentToAddFriendFragment()
        findNavController().navigate(action)
    }

    private fun handleLogOut() {
        activity?.viewModelStore?.clear()
        credentialsViewModel.logOut(requireContext())
        val action = MainFragmentDirections.actionMainFragmentToLoginFragment()
        findNavController().navigate(action)
    }

    private fun handleFriendList() {
        val action = MainFragmentDirections.actionMainFragmentToFriendListFragment()
        findNavController().navigate(action)
    }

    private fun handleSetPub() {
        val action = MainFragmentDirections.actionMainFragmentToSetPubFragment()
        findNavController().navigate(action)
    }
}