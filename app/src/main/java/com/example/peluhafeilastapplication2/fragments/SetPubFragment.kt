package com.example.peluhafeilastapplication2.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.PubApplication
import com.example.peluhafeilastapplication2.adapters.SetPubListItemAdapter
import com.example.peluhafeilastapplication2.databinding.FragmentSetPubBinding
import com.example.peluhafeilastapplication2.viewmodels.SetPubViewModel
import com.example.peluhafeilastapplication2.viewmodels.SetPubViewModelFactory
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModelFactory
import com.google.android.gms.location.LocationServices


class SetPubFragment : Fragment() {
    private lateinit var binding: FragmentSetPubBinding
    private val credentialsViewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }
    private val setPubViewModel: SetPubViewModel by activityViewModels {
        SetPubViewModelFactory(
            LocationServices.getFusedLocationProviderClient(requireActivity()),
            requireActivity().application,
            (activity?.application as PubApplication).database.userLocationDao(),
            (activity?.application as PubApplication).database.nearPubDao(),
            credentialsViewModel.credentials.value!!
        )
    }
    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_BACKGROUND_LOCATION, false) -> {
                // Precise location access granted.
            }
            else -> {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri: Uri = Uri.fromParts("package", context?.packageName, null)
                intent.data = uri
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSetPubBinding.inflate(inflater, container, false)
        observeViewModel()

        binding.findPubsButton.setOnClickListener { handleLocateMe() }

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewModel() {
        setPubViewModel.currentLocation.observe(viewLifecycleOwner) { value ->
            binding.currentCoordinates.text = "${value?.lat}  |  ${value?.lon}"
            binding.currentPubNameText.text = setPubViewModel.getCurrentPubName()
        }
        setPubViewModel.isLoading.observe(viewLifecycleOwner) {
            value -> binding.animationView.visibility = value
        }
        setPubViewModel.allPubs.observe(viewLifecycleOwner) {
            binding.setPubList.adapter = SetPubListItemAdapter(binding.root, viewLifecycleOwner, setPubViewModel, credentialsViewModel)
            binding.currentPubNameText.text = setPubViewModel.getCurrentPubName()
        }
    }

    private fun handleLocateMe() {
        if (checkBackgroundPermissions()) {
            if (Connection.isOnline(requireContext())) {
                setPubViewModel.locateMe()
            } else {
                android.app.AlertDialog.Builder(context).setTitle("Info").setMessage("NO Internet connection").create().show()
            }
        } else {
            permissionDialog()
        }
    }

    private fun permissionDialog() {
        val alertDialog: AlertDialog = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle("Background location needed")
                setMessage("Allow background location (All times) for detecting when you leave bar.")
                setPositiveButton("OK",
                    DialogInterface.OnClickListener { dialog, id ->
                        locationPermissionRequest.launch(
                            arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                        )
                    })
                setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id -> })
            }
            builder.create()
        }
        alertDialog.show()
    }

    private fun checkBackgroundPermissions(): Boolean {
        return ActivityCompat
            .checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
    }
}