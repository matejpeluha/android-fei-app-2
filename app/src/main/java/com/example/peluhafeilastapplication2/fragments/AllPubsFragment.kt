package com.example.peluhafeilastapplication2.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.PubApplication
import com.example.peluhafeilastapplication2.R
import com.example.peluhafeilastapplication2.adapters.PubDetailListItemAdapter
import com.example.peluhafeilastapplication2.databinding.FragmentAllPubsBinding
import com.example.peluhafeilastapplication2.model.enums.PubSort
import com.example.peluhafeilastapplication2.viewmodels.*
import com.google.android.gms.location.LocationServices

class AllPubsFragment : Fragment() {
    private lateinit var binding: FragmentAllPubsBinding
    private val credentialsViewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }
    private val pubDetailViewModel: PubDetailViewModel by activityViewModels {
        PubDetailViewModelFactory(
            (activity?.application as PubApplication).database.pubDetailDao(),
            (activity?.application as PubApplication).database.pubCounterDao(),
        )
    }
    private val setPubViewModel: SetPubViewModel by activityViewModels {
        SetPubViewModelFactory(
            LocationServices.getFusedLocationProviderClient(requireActivity()),
            requireActivity().application,
            (activity?.application as PubApplication).database.userLocationDao(),
            (activity?.application as PubApplication).database.nearPubDao(),
            credentialsViewModel.credentials.value!!
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAllPubsBinding.inflate(inflater, container, false)

        observeViewModel()

        binding.reloadButton.setOnClickListener { loadPubsFromServer() }
        binding.sortDistanceButton.setOnClickListener { pubDetailViewModel.setDistanceSortInfo() }
        binding.sortPubNameButton.setOnClickListener { pubDetailViewModel.setNameSortInfo() }
        binding.sortUsersCountButton.setOnClickListener { pubDetailViewModel.setUsersCountSortInfo() }

        loadPubsFromServer()

        return binding.root
    }

    private fun loadPubsFromServer() {
        if (Connection.isOnline(requireContext())) {
            pubDetailViewModel.loadPubsFromServer(credentialsViewModel.credentials.value!!)
        } else {
            AlertDialog.Builder(context).setTitle("Info").setMessage("NO Internet connection").create().show()
        }
    }

    private fun observeViewModel() {
        pubDetailViewModel.isLoading.observe(viewLifecycleOwner) {
            value -> binding.animationView.visibility = value
        }
        pubDetailViewModel.allPubs.observe(viewLifecycleOwner) {
            val sortedPubs = pubDetailViewModel.getSortedPubs(setPubViewModel.currentLocation.value)
            binding.pubList.adapter = PubDetailListItemAdapter(binding.root, pubDetailViewModel, sortedPubs, credentialsViewModel.credentials.value)
        }
        pubDetailViewModel.sortInfo.observe(viewLifecycleOwner) { value ->
            if ((value === PubSort.DISTANCE_ASC || value === PubSort.DISTANCE_DESC)
                && (setPubViewModel.currentLocation.value == null
                        || setPubViewModel.currentLocation.value!!.lat === null
                        || setPubViewModel.currentLocation.value!!.lon === null )
            ) {
                val action = AllPubsFragmentDirections.actionAllPubsFragmentToSetPubFragment()
                findNavController().navigate(action)
            } else {
                val sortedPubs = pubDetailViewModel.getSortedPubs(setPubViewModel.currentLocation.value)
                binding.pubList.adapter = PubDetailListItemAdapter(binding.root, pubDetailViewModel, sortedPubs, credentialsViewModel.credentials.value)
            }
        }
    }
}