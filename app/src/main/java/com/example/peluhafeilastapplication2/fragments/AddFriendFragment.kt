package com.example.peluhafeilastapplication2.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.PubApplication
import com.example.peluhafeilastapplication2.databinding.FragmentAddFriendBinding
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.example.peluhafeilastapplication2.viewmodels.FriendsViewModel
import com.example.peluhafeilastapplication2.viewmodels.FriendsViewModelFactory
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModelFactory

class AddFriendFragment : Fragment() {
    lateinit var binding: FragmentAddFriendBinding
    private val credentialsViewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }
    private val friendsViewModel: FriendsViewModel by activityViewModels {
        FriendsViewModelFactory(
            (activity?.application as PubApplication).database.friendDao(),
            credentialsViewModel.credentials.value!!.uid!!
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentAddFriendBinding.inflate(inflater, container, false)

        binding.addFriendButton.setOnClickListener { handleAddFriend() }
        observeViewModel()

        return binding.root
    }



    private fun handleAddFriend() {
        val credentials: UserCredentials = credentialsViewModel.credentials.value!!
        val name: String = binding.nameInput.text.toString()
        if (Connection.isOnline(requireContext())) {
            friendsViewModel.addFriend(name, credentials)
        } else {
            AlertDialog.Builder(context).setTitle("Info").setMessage("NO Internet connection").create().show()
        }
    }

    private fun observeViewModel() {
        friendsViewModel.usernameErrorVisibility.observe(viewLifecycleOwner) {
            value -> binding.addFriendError.visibility = value
        }
        friendsViewModel.serverAcceptRegistration.observe(viewLifecycleOwner) {
            value -> finishFriending(value)
        }
        friendsViewModel.isLoading.observe(viewLifecycleOwner) {
            value -> binding.animationView.visibility = value
        }
    }

    private fun finishFriending(isAccepted: Boolean) {
        if (isAccepted) {
//            val action = AddFriendFragmentDirections.actionAddFriendFragmentToMainFragment()
//            findNavController().navigate(action)
            findNavController().popBackStack()
            friendsViewModel.reloadServerAcceptRegistration()
            AlertDialog.Builder(context).setTitle("Info").setMessage("Friend added").create().show()
        }
    }
}