package com.example.peluhafeilastapplication2.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.peluhafeilastapplication2.PubApplication
import com.example.peluhafeilastapplication2.R
import com.example.peluhafeilastapplication2.databinding.FragmentPubDetailBinding
import com.example.peluhafeilastapplication2.model.database.PubDetailDataModel
import com.example.peluhafeilastapplication2.viewmodels.PubDetailViewModel
import com.example.peluhafeilastapplication2.viewmodels.PubDetailViewModelFactory

class PubDetailFragment : Fragment() {
    private lateinit var binding: FragmentPubDetailBinding
    private val pubDetailViewModel: PubDetailViewModel by activityViewModels {
        PubDetailViewModelFactory(
            (activity?.application as PubApplication).database.pubDetailDao(),
            (activity?.application as PubApplication).database.pubCounterDao(),
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPubDetailBinding.inflate(inflater, container, false)

        observeViewModel()

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewModel() {
        pubDetailViewModel.isLoading.observe(viewLifecycleOwner) {
            value -> binding.animationView.visibility = value
        }
        pubDetailViewModel.chosenPubDetail.observe(viewLifecycleOwner) {
            value -> handleChangePubDetail(value)
        }
        pubDetailViewModel.chosenPubCount.observe(viewLifecycleOwner) {
            value -> binding.userCountText.text = "Pocet pouzivatelov: $value"
        }
    }

    @SuppressLint("SetTextI18n")
    private fun handleChangePubDetail(pub: PubDetailDataModel) {
        binding.pubNameText.text = pub.name
        binding.amenityText.text = pub.amenity
        binding.latitudeText.text = "Lat: ${pub.lat}"
        binding.longitudeText.text = "Long: ${pub.lon}"
        binding.openingHoursText.text = "Otvorene: " + if (pub.opening_hours !== null) {
            pub.opening_hours
        } else {
            "?"
        }
        binding.websiteText.text = "Web: " + if (pub.website !== null) {
            pub.website
        } else {
            "?"
        }
        binding.phoneText.text = "Tel: " + if (pub.phone !== null) {
            pub.phone
        } else {
            "?"
        }


        initShowMapButton(pub.lat.toString(), pub.lon.toString())
        initPhoneButton(pub.phone)
        initWebButton(pub.website)
    }

    private fun initWebButton(web: String?) {
        if (web !== null) {
            binding.webButton.visibility = View.VISIBLE
            binding.webButton.setOnClickListener { handleOpenWebSite(web) }
        } else {
            binding.webButton.visibility = View.GONE
        }
    }

    private fun initPhoneButton(phoneNumber: String?) {
        if (phoneNumber !== null) {
            binding.phoneButton.visibility = View.VISIBLE
            binding.phoneButton.setOnClickListener { handleOpenPhoneNumber(phoneNumber) }
        } else {
            binding.phoneButton.visibility = View.GONE
        }
    }

    private fun initShowMapButton(latitude: String?, longitude: String?) {
        if (latitude !== null && longitude !== null) {
            binding.showMapButton.setOnClickListener {
                handleOnClickShowMapButton(latitude, longitude)
            }
        }
    }

    private fun handleOnClickShowMapButton(latitude: String, longitude: String) {
        // Create a Uri from an intent string. Use the result to create an Intent.
        val gmmIntentUri = Uri.parse("geo:0,0?q=$latitude,$longitude")

        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)

        // Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps")

        // Attempt to start an activity that can handle the Intent
        startActivity(mapIntent)
    }

    private fun handleOpenWebSite(uri: String) {
        val intentUri = Uri.parse(uri)
        val intent = Intent(Intent.ACTION_VIEW, intentUri)
        startActivity(intent)
    }

    private fun handleOpenPhoneNumber(phoneNumber: String) {
        val phone = phoneNumber.filter { !it.isWhitespace() }
        val intentUri = Uri.parse("tel:$phone")
        val intent = Intent(Intent.ACTION_DIAL, intentUri)
        startActivity(intent)
    }


}