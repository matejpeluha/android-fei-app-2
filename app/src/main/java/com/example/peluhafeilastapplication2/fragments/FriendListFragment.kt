package com.example.peluhafeilastapplication2.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.PubApplication
import com.example.peluhafeilastapplication2.adapters.FriendListItemAdapter
import com.example.peluhafeilastapplication2.databinding.FragmentFriendListBinding
import com.example.peluhafeilastapplication2.viewmodels.*


class FriendListFragment : Fragment() {
    private lateinit var binding: FragmentFriendListBinding
    private val credentialsViewModel: UserCredentialsViewModel by activityViewModels { UserCredentialsViewModelFactory() }
    private val friendsViewModel: FriendsViewModel by activityViewModels {
        FriendsViewModelFactory(
            (activity?.application as PubApplication).database.friendDao(),
            credentialsViewModel.credentials.value!!.uid!!
        )
    }
    private val pubDetailViewModel: PubDetailViewModel by activityViewModels {
        PubDetailViewModelFactory(
            (activity?.application as PubApplication).database.pubDetailDao(),
            (activity?.application as PubApplication).database.pubCounterDao(),
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentFriendListBinding.inflate(inflater, container, false)

        binding.addFriendButton.setOnClickListener { handleAddFriend() }
        binding.reloadButton.setOnClickListener { handleReloadServer() }

        updateFriendList()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModels()
        if (Connection.isOnline(requireContext())) {
            friendsViewModel.loadFriendListFromServer(credentialsViewModel.credentials.value!!)
        }
    }

    private fun updateFriendList() {
        binding.friendList.adapter =  FriendListItemAdapter(binding.root, friendsViewModel, credentialsViewModel, pubDetailViewModel)
    }

    private fun handleAddFriend() {
        val action = FriendListFragmentDirections.actionFriendListFragmentToAddFriendFragment()
        findNavController().navigate(action)
    }

    private fun observeViewModels() {
        friendsViewModel.allFriends.observe(viewLifecycleOwner) {
            updateFriendList()
        }
        friendsViewModel.isLoading.observe(viewLifecycleOwner) {
            value -> binding.animationView.visibility = value
        }
    }

    private fun handleReloadServer() {
        if (Connection.isOnline(requireContext())) {
            friendsViewModel.loadFriendListFromServer(credentialsViewModel.credentials.value!!)
        } else {
            AlertDialog.Builder(context).setTitle("Info").setMessage("NO Internet connection").create().show()
        }
    }
}