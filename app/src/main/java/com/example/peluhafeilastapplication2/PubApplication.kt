package com.example.peluhafeilastapplication2

import android.app.Application
import com.example.peluhafeilastapplication2.data.PubRoomDatabase

class PubApplication: Application() {
    val database: PubRoomDatabase by lazy {
        PubRoomDatabase.getDatabase(this)
    }
}