package com.example.peluhafeilastapplication2.model.apiResponse

data class AllPubs(
    val elements: Array<PubJson>
)
