package com.example.peluhafeilastapplication2.model.apiRequest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PinnedPub(
    @Json(name="id")
    val id: Long,

    @Json(name="name")
    val name: String,

    @Json(name="type")
    val type: String,

    @Json(name="lat")
    val latitude: Double,

    @Json(name="lon")
    val longitude: Double
)
