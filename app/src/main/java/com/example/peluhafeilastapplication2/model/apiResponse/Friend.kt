package com.example.peluhafeilastapplication2.model.apiResponse

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Friend (
    @Json(name="user_id")
    val userId: Int,

    @Json(name="user_name")
    val userName: String,

    @Json(name="bar_id")
    val barId: Long?,

    @Json(name="bar_name")
    val barName: String?,

    @Json(name="time")
    val time: String?,

    @Json(name="bar_lat")
    val barLatitude: String?,

    @Json(name="bar_lon")
    val barLongitude: String?
        )
