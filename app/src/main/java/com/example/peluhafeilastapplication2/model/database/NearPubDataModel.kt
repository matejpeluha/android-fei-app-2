package com.example.peluhafeilastapplication2.model.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "near_pub", primaryKeys = ["id", "user_id"])
data class NearPubDataModel(
    @ColumnInfo(name = "id")
    val id: Long,

    @ColumnInfo(name = "user_id")
    val userId: Int,

    @ColumnInfo(name = "latitude")
    val lat: Double,

    @ColumnInfo(name = "longitude")
    val lon: Double,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "amenity")
    val amenity: String?,

    @ColumnInfo(name = "opening_hours")
    val opening_hours: String?,

    @ColumnInfo(name = "website")
    val website: String?,

    @ColumnInfo(name = "phone")
    val phone: String?,

    @ColumnInfo(name = "distance")
    val distance: Float?,
): Parcelable
