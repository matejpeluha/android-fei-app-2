package com.example.peluhafeilastapplication2.model.apiResponse

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
data class Tags(
    val name: String?,
    val amenity: String?,
    val opening_hours: String?,
    val website: String?,
    val phone: String?
): Parcelable
