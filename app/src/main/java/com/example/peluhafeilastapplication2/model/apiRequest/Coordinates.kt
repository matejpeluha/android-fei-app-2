package com.example.peluhafeilastapplication2.model.apiRequest

data class Coordinates(
    val latitude: Double,
    val longitude: Double
)