package com.example.peluhafeilastapplication2.model.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "pub_counter", primaryKeys = ["id"])
data class PubCounterDataModel(
    @ColumnInfo(name = "id")
    val id: Long,

    @ColumnInfo(name = "latitude")
    val lat: Double,

    @ColumnInfo(name = "longitude")
    val lon: Double,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "users_count")
    var usersCount: Int
): Parcelable