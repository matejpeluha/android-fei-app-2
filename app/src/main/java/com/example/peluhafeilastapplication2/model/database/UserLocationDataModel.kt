package com.example.peluhafeilastapplication2.model.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "user_location", primaryKeys = ["user_id"])
data class UserLocationDataModel(
    @ColumnInfo(name = "user_id")
    val userId: Int,

    @ColumnInfo(name = "latitude")
    val lat: Double?,

    @ColumnInfo(name = "longitude")
    val lon: Double?,

    @ColumnInfo(name = "pub_id")
    var pubId: Long?
) : Parcelable
