package com.example.peluhafeilastapplication2.model.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "friend", primaryKeys = ["user_id", "friend_id"])
data class FriendDataModel(
    @ColumnInfo(name = "user_id")
    val userId: Int,

    @ColumnInfo(name = "friend_id")
    val friendId: Int,

    @ColumnInfo(name = "friend_name")
    val friendName: String?,

    @ColumnInfo(name = "bar_id")
    val barId: Long?,

    @ColumnInfo(name = "bar_name")
    val barName: String?,

    @ColumnInfo(name = "time")
    val time: String?,

    @ColumnInfo(name = "bar_lat")
    val barLatitude: String?,

    @ColumnInfo(name = "bar_lon")
    val barLongitude: String?,
) : Parcelable
