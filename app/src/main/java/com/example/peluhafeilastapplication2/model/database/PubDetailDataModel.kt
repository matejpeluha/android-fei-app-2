package com.example.peluhafeilastapplication2.model.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "pub_detail", primaryKeys = ["id"])
data class PubDetailDataModel(
    @ColumnInfo(name = "id")
    val id: Long,

    @ColumnInfo(name = "latitude")
    val lat: Double,

    @ColumnInfo(name = "longitude")
    val lon: Double,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "amenity")
    var amenity: String?,

    @ColumnInfo(name = "opening_hours")
    var opening_hours: String?,

    @ColumnInfo(name = "website")
    var website: String?,

    @ColumnInfo(name = "phone")
    var phone: String?,
): Parcelable