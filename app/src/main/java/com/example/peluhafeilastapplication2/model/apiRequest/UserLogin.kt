package com.example.peluhafeilastapplication2.model.apiRequest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserLogin(
    @Json(name="name")
    val name: String,
    @Json(name="password")
    val password: String,
    val secondPassword: String?
)