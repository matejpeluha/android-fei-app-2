package com.example.peluhafeilastapplication2.model.apiResponse

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserCredentials(
    @Json(name="uid")
    val uid: Int?,
    @Json(name="access")
    val access: String?,
    @Json(name="refresh")
    val refresh: String?
)