package com.example.peluhafeilastapplication2.model.apiRequest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FoundPubList (
    @Json(name="collection")
    val collection: String = "bars",

    @Json(name="database")
    val database: String = "mobvapp",

    @Json(name="dataSource")
    val dataSource: String = "Cluster0"
)