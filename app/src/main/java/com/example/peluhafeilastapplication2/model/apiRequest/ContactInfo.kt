package com.example.peluhafeilastapplication2.model.apiRequest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ContactInfo(
    @Json(name="contact")
    val contact: String
)
