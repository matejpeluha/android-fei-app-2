package com.example.peluhafeilastapplication2.model.apiResponse

import android.location.Location
import android.os.Parcelable
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel
import com.example.peluhafeilastapplication2.model.database.PubDetailDataModel
import com.example.peluhafeilastapplication2.model.database.UserLocationDataModel
import kotlinx.parcelize.Parcelize

@Parcelize
data class PubJson(
    val id: Long,
    val lat: Double,
    val lon: Double,
    val tags: Tags
): Parcelable {
    fun toDataModelWithDistance(userLocation: UserLocationDataModel): NearPubDataModel {
        val results = FloatArray(1)
        Location.distanceBetween(userLocation.lat!!, userLocation.lon!!, lat, lon, results)
        return NearPubDataModel(
            id = id,
            userId  = userLocation.userId,
            lat = lat,
            lon = lon,
            name = tags.name,
            amenity = tags.amenity,
            opening_hours = tags.opening_hours,
            website = tags.website,
            phone = tags.phone,
            distance = results[0]
        )
    }

    fun toPubDetailDataModel(): PubDetailDataModel {
        return PubDetailDataModel(
            id = id,
            lat = lat,
            lon = lon,
            name = tags.name,
            amenity = tags.amenity,
            opening_hours = tags.opening_hours,
            website = tags.website,
            phone = tags.phone,
        )
    }
}