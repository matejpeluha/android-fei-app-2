package com.example.peluhafeilastapplication2.model.enums

enum class PubSort {
    NONE, DISTANCE_ASC, DISTANCE_DESC, NAME_ASC, NAME_DESC, USERS_ASC, USERS_DESC
}