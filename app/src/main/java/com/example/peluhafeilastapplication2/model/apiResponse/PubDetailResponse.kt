package com.example.peluhafeilastapplication2.model.apiResponse

import com.example.peluhafeilastapplication2.model.database.PubCounterDataModel
import com.example.peluhafeilastapplication2.model.database.PubDetailDataModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PubDetailResponse(
    @Json(name="bar_id")
    val id: Long,

    @Json(name="bar_name")
    val pubName: String,

    @Json(name="lat")
    val latitude: Double,

    @Json(name="lon")
    val longitude: Double,

    @Json(name="bar_type")
    val amenity: String,

    @Json(name="users")
    val usersCount: Int,

    @Json(name="last_update")
    val lastUpdate: String,
) {
    fun toPubCounterDetailDataModel(): PubCounterDataModel {
        return PubCounterDataModel(
            id = id,
            lat = latitude,
            lon = longitude,
            name = pubName,
            usersCount = usersCount
        )
    }

    fun toPubDetailDataModel(): PubDetailDataModel {
        return PubDetailDataModel(
            id = id,
            lat = latitude,
            lon = longitude,
            name = pubName,
            amenity = amenity,
            opening_hours = null,
            website = null,
            phone = null
        )
    }
}