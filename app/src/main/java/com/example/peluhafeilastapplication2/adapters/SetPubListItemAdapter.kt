package com.example.peluhafeilastapplication2.adapters

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.compose.runtime.snapshots.Snapshot.Companion.observe
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.R
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel
import com.example.peluhafeilastapplication2.model.database.UserLocationDataModel
import com.example.peluhafeilastapplication2.viewmodels.FriendsViewModel
import com.example.peluhafeilastapplication2.viewmodels.SetPubViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel

class SetPubListItemAdapter(
    public val view: View,
    public val viewLifecycleOwner: LifecycleOwner,
    private val setPubViewModel: SetPubViewModel,
    private val credentialsViewModel: UserCredentialsViewModel
): RecyclerView.Adapter<SetPubListItemAdapter.ItemViewHolder>() {

    class ItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val pubName: TextView = view.findViewById(R.id.setPubItemPubNameText)
        val distance: TextView = view.findViewById(R.id.setPubItemDistanceText)
        val locatedPubCheck: LottieAnimationView = view.findViewById(R.id.locatedPub)
        val setPubButton: Button = view.findViewById(R.id.setPubButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  SetPubListItemAdapter.ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.set_pub_list_item, parent, false)
        return  SetPubListItemAdapter.ItemViewHolder(adapterLayout)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder:  SetPubListItemAdapter.ItemViewHolder, position: Int) {
        val item = setPubViewModel.allPubs.value!![position]
        holder.pubName.text = item.name
        holder.distance.text = "${item.distance?.toInt().toString()}m"

        setIcon(holder, item, setPubViewModel.currentLocation.value)

        setPubViewModel.currentLocation.observe(viewLifecycleOwner) { value ->
            setIcon(holder, item, value)
        }

        holder.setPubButton.setOnClickListener {
            if (Connection.isOnline(view.context)) {
                setPubViewModel.setCurrentPub(item)
            } else {
                AlertDialog.Builder(view.context).setTitle("Info").setMessage("NO Internet connection").create().show()
            }
        }
    }

    private fun setIcon(holder:  SetPubListItemAdapter.ItemViewHolder, rowPub: NearPubDataModel, currentLocation: UserLocationDataModel?) {
        if (currentLocation !== null && currentLocation.pubId !== null){
            if (currentLocation.pubId!! === rowPub.id) {
                holder.locatedPubCheck.visibility = View.VISIBLE
            } else {
                holder.locatedPubCheck.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return setPubViewModel.allPubs.value!!.size
    }
}