package com.example.peluhafeilastapplication2.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.peluhafeilastapplication2.R
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel


class PubItemAdapter(
    public val view: View,
    private val dataset: Array<NearPubDataModel>
    ) : RecyclerView.Adapter<PubItemAdapter.ItemViewHolder>() {


    class ItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val pubNameButton: TextView = view.findViewById(R.id.pub_item_title)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.pub_list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.pubNameButton.text = item.name
        holder.pubNameButton.setOnClickListener { handleOnClickPubButton(item, holder) }
    }

    private fun handleOnClickPubButton(item: NearPubDataModel, holder: ItemViewHolder) {
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}