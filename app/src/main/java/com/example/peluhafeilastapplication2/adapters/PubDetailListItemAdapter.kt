package com.example.peluhafeilastapplication2.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.example.peluhafeilastapplication2.R
import com.example.peluhafeilastapplication2.fragments.AllPubsFragmentDirections
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.example.peluhafeilastapplication2.model.database.PubCounterDataModel
import com.example.peluhafeilastapplication2.viewmodels.PubDetailViewModel

class PubDetailListItemAdapter(
    public val view: View,
    private val pubDetailViewModel: PubDetailViewModel,
    private val sortedPubs: Array<PubCounterDataModel>,
    private val credentials: UserCredentials?
): RecyclerView.Adapter<PubDetailListItemAdapter.ItemViewHolder>() {

    class ItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val pubName: TextView = view.findViewById(R.id.pubDetailListItemPubNameText)
        val usersCount: TextView = view.findViewById(R.id.pubDetailListItemUsersCountText)
        val openPubButton: Button = view.findViewById(R.id.openPubDetailButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  PubDetailListItemAdapter.ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.pub_detail_list_item, parent, false)
        return  PubDetailListItemAdapter.ItemViewHolder(adapterLayout)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PubDetailListItemAdapter.ItemViewHolder, position: Int) {
        val item: PubCounterDataModel = sortedPubs[position]
        holder.pubName.text = item.name
        holder.usersCount.text = "Users: ${item.usersCount}"

        holder.openPubButton.setOnClickListener {
            pubDetailViewModel.setChosenPub(item.id, credentials, view.context)
            val action = AllPubsFragmentDirections.actionAllPubsFragmentToPubDetailFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        if (pubDetailViewModel.allPubs.value === null) {
            return 0
        }
        return pubDetailViewModel.allPubs.value!!.size
    }
}