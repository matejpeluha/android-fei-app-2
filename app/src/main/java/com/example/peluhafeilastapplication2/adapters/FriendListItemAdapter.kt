package com.example.peluhafeilastapplication2.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.example.peluhafeilastapplication2.R
import com.example.peluhafeilastapplication2.fragments.FriendListFragmentDirections
import com.example.peluhafeilastapplication2.model.apiRequest.ContactInfo
import com.example.peluhafeilastapplication2.model.database.FriendDataModel
import com.example.peluhafeilastapplication2.viewmodels.FriendsViewModel
import com.example.peluhafeilastapplication2.viewmodels.PubDetailViewModel
import com.example.peluhafeilastapplication2.viewmodels.UserCredentialsViewModel

class FriendListItemAdapter(
    public val view: View,
    private val friendsViewModel: FriendsViewModel,
    private val credentialsViewModel: UserCredentialsViewModel,
    private val pubDetailViewModel: PubDetailViewModel
) : RecyclerView.Adapter<FriendListItemAdapter.ItemViewHolder>() {

    class ItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val friendName: TextView = view.findViewById(R.id.friendNameText)
        val pubName: TextView = view.findViewById(R.id.pubNameText)
        val openPubDetailButton: Button = view.findViewById(R.id.openFriendPubDetailButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  FriendListItemAdapter.ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.friend_list_item, parent, false)
        return  FriendListItemAdapter.ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder:  FriendListItemAdapter.ItemViewHolder, position: Int) {
        if (friendsViewModel.allFriends.value !== null) {
            val item = friendsViewModel.allFriends.value!![position]
            holder.friendName.text = item.friendName
            holder.pubName.text = item.barName

            if (item.barId !== null) {
                holder.openPubDetailButton.setOnClickListener { handleOpenPubDetailButton(item.barId) }
            }
        }
    }

    private fun handleOpenPubDetailButton(pubId: Long) {
        pubDetailViewModel.setChosenPub(pubId, credentialsViewModel.credentials.value, view.context)
        val action = FriendListFragmentDirections.actionFriendListFragmentToPubDetailFragment()
        view.findNavController().navigate(action)
    }

    override fun getItemCount(): Int {
        if (friendsViewModel.allFriends.value === null){
            return 0
        }
        return friendsViewModel.allFriends.value!!.size
    }
}