package com.example.peluhafeilastapplication2.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.example.peluhafeilastapplication2.data.NearPubDao
import com.example.peluhafeilastapplication2.model.*
import com.example.peluhafeilastapplication2.model.apiRequest.FoundPubList
import com.example.peluhafeilastapplication2.model.apiResponse.PubJson
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel
import com.example.peluhafeilastapplication2.services.PubListApi
import kotlinx.coroutines.launch

class PubListViewModel(private var nearPubDao: NearPubDao) : ViewModel() {
    private var _allPubs: LiveData<Array<NearPubDataModel>> = nearPubDao.getAllPubs()

    val allPubs: LiveData<Array<NearPubDataModel>>
        get() = _allPubs

    private var _order: MutableLiveData<Int> = MutableLiveData(0)

    val order: MutableLiveData<Int>
        get() = _order


    init {
        Log.d("PubListViewModel:", "Created!")
        loadPubList()
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("PubListViewModel:", "Destroyed!")
    }

    fun loadPubList() {
        val foundPubListApiBody = FoundPubList()
        viewModelScope.launch {
            try {
                val response = PubListApi.retrofitService.getPubList(foundPubListApiBody)
                for (pubJson in response.elements) {
                    val pub = transformPubJsonToPub(pubJson)
                    updateOrInsert(pub)
                }
            } catch (e: java.lang.Exception) {
                Log.e("error", e.toString())
            }
        }
    }

    private fun transformPubJsonToPub(pubJson: PubJson): NearPubDataModel {
        return NearPubDataModel(
            pubJson.id,
            0,
            pubJson.lat,
            pubJson.lon,
            pubJson.tags.name,
            pubJson.tags.amenity,
            pubJson.tags.opening_hours,
            pubJson.tags.website,
            pubJson.tags.phone,
            null
        )
    }

    private suspend fun updateOrInsert(nearPubDataModel: NearPubDataModel) {
//        val updatedCount = nearPubDao.update(nearPubDataModel)
//        if (updatedCount == 0) {
//            nearPubDao.insert(nearPubDataModel)
//        }
    }

    public fun getSortedPubs(): Array<NearPubDataModel>? {
        if (order.value == 0) {
            return allPubs.value
        }

        val sortedPubs = allPubs.value

        sortedPubs?.sortWith { first: NearPubDataModel, second: NearPubDataModel ->
            if (second.name === null && first.name === null) {
                return@sortWith 0
            } else if ((second.name === null && order.value!! >= 0) || (first.name === null && order.value!! < 0)) {
                return@sortWith -1
            } else if ((first.name === null && order.value!! >= 0) || (second.name === null && order.value!! < 0)) {
                return@sortWith 1
            } else if (second.name !== null && first.name !== null && order.value!! >= 0) {
                first.name.compareTo(second.name)
            } else if (second.name !== null && first.name !== null && order.value!! < 0) {
                second.name.compareTo(first.name)
            } else {
                return@sortWith 0
            }

        }

        return sortedPubs
    }

    public fun deletePub(nearPubDataModel: NearPubDataModel) {
        viewModelScope.launch {
            nearPubDao.delete(nearPubDataModel)
        }
    }

    public fun changeOrder() {
        if (order.value!! >= 0) {
            _order.value = -1
        } else {
            _order.value = 1
        }
    }
}


class PubListViewModelFactory(private val nearPubDao: NearPubDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PubListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PubListViewModel(nearPubDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}