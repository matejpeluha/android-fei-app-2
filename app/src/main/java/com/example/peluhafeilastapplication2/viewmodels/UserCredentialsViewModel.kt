package com.example.peluhafeilastapplication2.viewmodels

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.peluhafeilastapplication2.data.PreferenceData
import com.example.peluhafeilastapplication2.model.apiRequest.UserLogin
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.example.peluhafeilastapplication2.services.UserApi
import kotlinx.coroutines.launch
import java.security.MessageDigest
import kotlin.experimental.and

class UserCredentialsViewModel: ViewModel() {
    private var _usernameErrorVisibility = MutableLiveData<Int>(View.GONE)
    val usernameErrorVisibility: MutableLiveData<Int>
        get() = _usernameErrorVisibility

    private var _passwordErrorVisibility = MutableLiveData<Int>(View.GONE)
    val passwordErrorVisibility: MutableLiveData<Int>
        get() = _passwordErrorVisibility

    private var _secondPasswordErrorVisibility = MutableLiveData<Int>(View.GONE)
    val secondPasswordErrorVisibility: MutableLiveData<Int>
        get() = _secondPasswordErrorVisibility

    private var _serverErrorVisibility = MutableLiveData<Int>(View.GONE)
    val serverErrorVisibility: MutableLiveData<Int>
        get() = _serverErrorVisibility

    private var _serverAccept = MutableLiveData<Boolean>(false)
    val serverAccept: MutableLiveData<Boolean>
        get() = _serverAccept

    private var _isLoading = MutableLiveData<Int>(View.GONE)
    val isLoading: MutableLiveData<Int>
        get() = _isLoading

    private var _credentials = MutableLiveData<UserCredentials>()
    val credentials: MutableLiveData<UserCredentials>
        get() = _credentials

    public fun tryAutoLogin(context: Context) {
        _isLoading.value = View.VISIBLE
        val oldCredentials: UserCredentials? = PreferenceData.getInstance().getUserItem(context)
        if (oldCredentials !== null && oldCredentials.uid !== null) {
            refreshCredentials(oldCredentials)
        }
        _isLoading.value = View.GONE
    }

    private fun refreshCredentials(oldCredentials: UserCredentials) {
        viewModelScope.launch {
            try {
                val userId: Int = oldCredentials.uid!!
                val newCredentials = UserApi.retrofitService.refreshCredentials(userId, oldCredentials)
                _credentials.value = newCredentials
                _serverAccept.value = true
            } catch (e: java.lang.Exception) {
                Log.e("error", e.toString())
            }
        }
    }

    public fun clientValidation(user: UserLogin): Boolean {
        var error = false
        if (user.password.isEmpty()) {
            _passwordErrorVisibility.value = View.VISIBLE
            _serverErrorVisibility.value = View.GONE
            error = true
        } else {
            _passwordErrorVisibility.value = View.GONE
        }
        if (user.name.isEmpty()) {
            _usernameErrorVisibility.value = View.VISIBLE
            _serverErrorVisibility.value = View.GONE
            error = true
        } else {
            _usernameErrorVisibility.value = View.GONE
        }
        if (user.secondPassword !=null && (user.secondPassword != user.password || user.secondPassword.isEmpty())) {
            _secondPasswordErrorVisibility.value = View.VISIBLE
            _serverErrorVisibility.value = View.GONE
            error = true
        } else {
            _secondPasswordErrorVisibility.value = View.GONE
        }
        return error
    }

    public fun registerUser(user: UserLogin) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                val response: UserCredentials = UserApi.retrofitService.registerUser(user)
                _passwordErrorVisibility.value = View.GONE
                _secondPasswordErrorVisibility.value = View.GONE
                _usernameErrorVisibility.value = View.GONE
                if (response.uid == -1) {
                    _serverErrorVisibility.value = View.VISIBLE
                    _serverAccept.value = false
                } else {
                    _serverErrorVisibility.value = View.GONE
                    _serverAccept.value = true
                }
            } catch (e: java.lang.Exception) {
                Log.e("error", e.toString())
            }
            _isLoading.value = View.GONE
        }
    }

    public fun loginUser(user: UserLogin, context: Context) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                val response: UserCredentials = UserApi.retrofitService.loginUser(user)
                _passwordErrorVisibility.value = View.GONE
                _usernameErrorVisibility.value = View.GONE
                if (response.uid == -1) {
                    _serverErrorVisibility.value = View.VISIBLE
                    _serverAccept.value = false
                } else {
                    _serverErrorVisibility.value = View.GONE
                    _serverAccept.value = true
                    _credentials.value = response
                    PreferenceData.getInstance().clearData(context)
                    PreferenceData.getInstance().putUserItem(context, response)
                }
                Log.d("BEARER:", credentials.value?.access.toString())
                Log.d("USER", credentials.value?.uid.toString())
            } catch (e: java.lang.Exception) {
                Log.e("error", e.toString())
            }
            _isLoading.value = View.GONE
        }
    }

    fun hashPassword(password: String): String {
        val md: MessageDigest = MessageDigest.getInstance("SHA-512")
        val bytes: ByteArray = md.digest(password.toByteArray())
        val sb = StringBuilder()
        for (i in bytes.indices) {
            sb.append(
                ((bytes[i] and 0xff.toByte()) + 0x100).toString(16)
                    .substring(1)
            )
        }
        return sb.toString()
    }

    fun reloadServerAcceptRegistration() {
        _serverAccept.value = false
    }

    fun logOut(context: Context) {
        reloadServerAcceptRegistration()
        credentials.value = null
        PreferenceData.getInstance().clearData(context)
    }
}

class UserCredentialsViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserCredentialsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserCredentialsViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}