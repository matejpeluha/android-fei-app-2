package com.example.peluhafeilastapplication2.viewmodels

import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.example.peluhafeilastapplication2.data.FriendDao
import com.example.peluhafeilastapplication2.model.apiRequest.ContactInfo
import com.example.peluhafeilastapplication2.model.apiResponse.Friend
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.example.peluhafeilastapplication2.model.database.FriendDataModel
import com.example.peluhafeilastapplication2.services.ContactApi
import kotlinx.coroutines.launch

class FriendsViewModel(private val friendDao: FriendDao, private val userId: Int): ViewModel() {
    private var _allFriends: LiveData<Array<FriendDataModel>> = friendDao.getAllFriends(userId)
    val allFriends: LiveData<Array<FriendDataModel>>
        get() = _allFriends

    private var _usernameErrorVisibility = MutableLiveData<Int>(View.GONE)
    val usernameErrorVisibility: MutableLiveData<Int>
        get() = _usernameErrorVisibility

    private var _serverAcceptAdd = MutableLiveData<Boolean>(false)
    val serverAcceptRegistration: MutableLiveData<Boolean>
        get() = _serverAcceptAdd

    private var _isLoading = MutableLiveData<Int>(View.GONE)
    val isLoading: MutableLiveData<Int>
        get() = _isLoading

    fun  loadFriendListFromServer(credentials: UserCredentials) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                val userId = credentials.uid!!
                val bearerToken = "Bearer ${credentials.access}"
                val response = ContactApi.retrofitService.getFriends(userId, bearerToken)
                Log.d("RESPONSE SIZE", response.size.toString())
                for (friend in response) {
                    val friendDataModel = friendToDataModel(friend, userId)
                    updateOrInsert(friendDataModel)
                }
            } catch (e: retrofit2.HttpException) {
                Log.d("ERROR:", e.message().toString())
            }
            _isLoading.value = View.GONE
        }
    }

    private fun friendToDataModel(friend: Friend, userId: Int): FriendDataModel {
        return FriendDataModel(
            userId = userId,
            friendId = friend.userId,
            friendName = friend.userName,
            barId = friend.barId,
            barName = friend.barName,
            time = friend.time,
            barLatitude = friend.barLatitude,
            barLongitude = friend.barLongitude
        )
    }

    private suspend fun updateOrInsert(friendDataModel: FriendDataModel) {
        val updateCount = friendDao.update(friendDataModel)
        if (updateCount == 0) {
            friendDao.insert(friendDataModel)
        }
    }

    fun addFriend(friendName: String, credentials: UserCredentials) {
        if (friendName.isEmpty()) {
            _usernameErrorVisibility.value = View.VISIBLE
            return
        }
        val contact = ContactInfo(friendName)
        addFriendOnServer(contact, credentials)
    }

    private fun addFriendOnServer(contact: ContactInfo, credentials: UserCredentials) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                val userId = credentials.uid!!
                val bearerToken = "Bearer ${credentials.access}"
                ContactApi.retrofitService.addFriend(userId, bearerToken, contact)
                _usernameErrorVisibility.value = View.GONE
                _serverAcceptAdd.value = true
                loadFriendListFromServer(credentials)
            } catch (e: retrofit2.HttpException) {
                if (e.response()?.code() == 500) {
                    _usernameErrorVisibility.value = View.VISIBLE
                }
                Log.d("ERROR ADD FRIEND ON SERVER:", e.message().toString())
            }
            _isLoading.value = View.GONE
        }
    }

    fun removeFriendOnServer(friend: FriendDataModel, credentials: UserCredentials) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                val userId = credentials.uid!!
                val bearerToken = "Bearer ${credentials.access}"
                val contact = ContactInfo(friend.friendName!!)
                friendDao.delete(friend)
                ContactApi.retrofitService.removeFriend(userId, bearerToken, contact)
            } catch (e: retrofit2.HttpException) {
                Log.d("ERROR:", e.message())
            } finally {

            }
            _isLoading.value = View.GONE
        }
        loadFriendListFromServer(credentials)
    }

    fun reloadServerAcceptRegistration() {
        _serverAcceptAdd.value = false
    }
}

class FriendsViewModelFactory(private val friendDao: FriendDao, private val userId: Int)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FriendsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return FriendsViewModel(friendDao, userId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}