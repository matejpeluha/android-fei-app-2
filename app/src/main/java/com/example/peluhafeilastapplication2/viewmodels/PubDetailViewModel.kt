package com.example.peluhafeilastapplication2.viewmodels

import android.content.Context
import android.location.Location
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.example.peluhafeilastapplication2.Connection
import com.example.peluhafeilastapplication2.data.PubCounterDao
import com.example.peluhafeilastapplication2.data.PubDetailDao
import com.example.peluhafeilastapplication2.model.apiResponse.AllPubs
import com.example.peluhafeilastapplication2.model.apiResponse.PubDetailResponse
import com.example.peluhafeilastapplication2.model.apiResponse.PubJson
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.example.peluhafeilastapplication2.model.database.PubCounterDataModel
import com.example.peluhafeilastapplication2.model.database.PubDetailDataModel
import com.example.peluhafeilastapplication2.model.database.UserLocationDataModel
import com.example.peluhafeilastapplication2.model.enums.PubSort
import com.example.peluhafeilastapplication2.services.OverpassApi
import com.example.peluhafeilastapplication2.services.PubApi
import kotlinx.coroutines.launch


class PubDetailViewModel(
    private val pubDetailDao: PubDetailDao,
    private val pubCounterDao: PubCounterDao
) : ViewModel() {
    private var _allPubs = pubCounterDao.getAll()
    val allPubs: LiveData<Array<PubCounterDataModel>>
        get() = _allPubs

    private var _sortInfo = MutableLiveData(PubSort.NONE)
    val sortInfo: MutableLiveData<PubSort>
        get() = _sortInfo

    private var _isLoading = MutableLiveData<Int>(View.GONE)
    val isLoading: MutableLiveData<Int>
        get() = _isLoading

    private var _chosenPubDetail = MutableLiveData<PubDetailDataModel>()
    val chosenPubDetail: MutableLiveData<PubDetailDataModel>
        get() = _chosenPubDetail

    private var _chosenPubCount = MutableLiveData<Int>()
    val chosenPubCount: MutableLiveData<Int>
        get() = _chosenPubCount

    fun setChosenPub(pubId: Long, credentials: UserCredentials?, context: Context) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                if (credentials !== null && Connection.isOnline(context)) {
                    loadPubsFromServer(credentials)
                }

                if (Connection.isOnline(context)) {
                    val getPubQuery: String = OverpassApi.getPubQuery(pubId)
                    val response: AllPubs = OverpassApi.retrofitService.getResponse(getPubQuery)

                    if (response.elements.isNotEmpty()) {
                        val pubDetail: PubJson = response.elements[0]
                        val pubDetailDataModel = pubDetail.toPubDetailDataModel()
                        pubDetailDao.insert(pubDetailDataModel)
                    }
                }

                val detail: PubDetailDataModel = pubDetailDao.getPub(pubId)
                _chosenPubDetail.value = detail
                val counter: PubCounterDataModel = pubCounterDao.getOne(pubId)
                _chosenPubCount.value = counter.usersCount
            } catch (e: Exception) {
                Log.d("ERROR set chosen pub:", e.message.toString())
            }
            _isLoading.value = View.GONE
        }
    }

    fun loadPubsFromServer(credentials: UserCredentials) {
        viewModelScope.launch {
            _isLoading.value = View.VISIBLE
            try {
                val userId = credentials.uid!!
                val bearerToken = "Bearer ${credentials.access}"
                val response = PubApi.retrofitService.getLocatedPubs(userId, bearerToken)
                insertDataModels(response)
            } catch (e: Exception) {
                Log.d("ERROR load pubs from server:", e.message.toString())
            }
            _isLoading.value = View.GONE
        }
    }

    private suspend fun insertDataModels(pubsResponse: Array<PubDetailResponse>){
        var details = arrayOf<PubDetailDataModel>()
        var counters = arrayOf<PubCounterDataModel>()
        for (pub: PubDetailResponse in pubsResponse) {
            val detailDataModel = pub.toPubDetailDataModel()
            details += detailDataModel
            val counterDataModel = pub.toPubCounterDetailDataModel()
            counters += counterDataModel
        }
        pubDetailDao.insertAll(details)
        pubCounterDao.deleteAll()
        pubCounterDao.insertAll(counters)
    }

    fun setNameSortInfo() {
        if (sortInfo.value === PubSort.NAME_ASC) {
            _sortInfo.value = PubSort.NAME_DESC
        } else {
            _sortInfo.value = PubSort.NAME_ASC
        }
    }

    fun setDistanceSortInfo() {
        if (sortInfo.value === PubSort.DISTANCE_ASC) {
            _sortInfo.value = PubSort.DISTANCE_DESC
        } else {
            _sortInfo.value = PubSort.DISTANCE_ASC
        }
    }

    fun setUsersCountSortInfo() {
        if (sortInfo.value === PubSort.USERS_DESC) {
            _sortInfo.value = PubSort.USERS_ASC
        } else {
            _sortInfo.value = PubSort.USERS_DESC
        }
    }


    fun getSortedPubs(userLocation: UserLocationDataModel?): Array<PubCounterDataModel> {
        if (allPubs.value == null) {
            return arrayOf<PubCounterDataModel>()
        }
        val sortedPubs: Array<PubCounterDataModel> = allPubs.value!!

        if (sortInfo.value === PubSort.USERS_ASC) {
            sortedPubs.sortWith { first: PubCounterDataModel, second: PubCounterDataModel ->
                first.usersCount.minus(second.usersCount)
            }
        } else if (sortInfo.value === PubSort.USERS_DESC) {
            sortedPubs.sortWith { first: PubCounterDataModel, second: PubCounterDataModel ->
                second.usersCount.minus(first.usersCount)
            }
        } else if (sortInfo.value === PubSort.NAME_ASC) {
            sortedPubs.sortBy { it.name?.lowercase() }
        } else if (sortInfo.value === PubSort.NAME_DESC) {
            sortedPubs.sortByDescending { it.name?.lowercase() }
        } else if (sortInfo.value === PubSort.DISTANCE_ASC && userLocation !== null) {
            sortedPubs.sortWith { first: PubCounterDataModel, second: PubCounterDataModel ->
                if (userLocation.lat !== null && userLocation.lon !== null) {
                    val firstResults = FloatArray(1)
                    val secondResults = FloatArray(1)
                    Location.distanceBetween(userLocation.lat, userLocation.lon, first.lat, first.lon, firstResults)
                    Location.distanceBetween(userLocation.lat, userLocation.lon, second.lat, second.lon, secondResults)
                    (firstResults[0] - secondResults[0]).toInt()
                } else {
                    0
                }
            }
        } else if (sortInfo.value === PubSort.DISTANCE_DESC && userLocation !== null) {
            sortedPubs.sortWith { first: PubCounterDataModel, second: PubCounterDataModel ->
                if (userLocation.lat !== null && userLocation.lon !== null) {
                    val firstResults = FloatArray(1)
                    val secondResults = FloatArray(1)
                    Location.distanceBetween(userLocation.lat, userLocation.lon, first.lat, first.lon, firstResults)
                    Location.distanceBetween(userLocation.lat, userLocation.lon, second.lat, second.lon, secondResults)
                    (secondResults[0] - firstResults[0]).toInt()
                } else {
                    0
                }
            }
        }

        return sortedPubs
    }
}

class PubDetailViewModelFactory(private val pubDetailDao: PubDetailDao, private val pubCounterDao: PubCounterDao)
    : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PubDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PubDetailViewModel(pubDetailDao, pubCounterDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}