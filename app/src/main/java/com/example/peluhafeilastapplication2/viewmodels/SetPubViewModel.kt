package com.example.peluhafeilastapplication2.viewmodels

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.pm.PackageManager
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.*
import com.example.peluhafeilastapplication2.data.NearPubDao
import com.example.peluhafeilastapplication2.data.UserLocationDao
import com.example.peluhafeilastapplication2.model.apiRequest.PinnedPub
import com.example.peluhafeilastapplication2.model.apiResponse.PubJson
import com.example.peluhafeilastapplication2.model.apiResponse.UserCredentials
import com.example.peluhafeilastapplication2.model.database.NearPubDataModel
import com.example.peluhafeilastapplication2.model.database.UserLocationDataModel
import com.example.peluhafeilastapplication2.services.OverpassApi
import com.example.peluhafeilastapplication2.services.PubApi
import com.google.android.gms.location.CurrentLocationRequest
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.coroutines.launch

class SetPubViewModel(
    private val fusedLocationClient: FusedLocationProviderClient,
    private val application: Application,
    private val userLocationDao: UserLocationDao,
    private val nearPubDao: NearPubDao,
    private val credentials: UserCredentials
    ): ViewModel() {
    private var _currentLocation = userLocationDao.getLocation(credentials.uid!!)
    val currentLocation: LiveData<UserLocationDataModel>
        get() = _currentLocation

    private var _allPubs = nearPubDao.getAllPubsForUser(credentials.uid!!)
    val allPubs: LiveData<Array<NearPubDataModel>>
        get() = _allPubs

    private var _isLoading = MutableLiveData<Int>(View.GONE)
    val isLoading: MutableLiveData<Int>
        get() = _isLoading

    @SuppressLint("MissingPermission")
    fun locateMe() {
        if (checkPermissions()) {
            _isLoading.value = View.VISIBLE
            val locationRequest = CurrentLocationRequest
                .Builder()
                .setDurationMillis(30000)
                .setMaxUpdateAgeMillis(60000)
                .build()
            fusedLocationClient
                .getCurrentLocation(locationRequest, null)
                .addOnSuccessListener {
                    it?.let {
                        viewModelScope.launch {
                            try {
                                val userLocation = UserLocationDataModel(
                                    credentials.uid!!,
                                    it.latitude,
                                    it.longitude,
                                    null
                                )
                                userLocationDao.insert(userLocation)
                                findPubsByLocation(userLocation)
                            } catch (e: Exception) {
                                Log.d("ERROR add my lcoation:", e.message.toString())
                            }
                        }
                    }
                }
        }
    }

    private fun findPubsByLocation(userLocation: UserLocationDataModel) {
        viewModelScope.launch {
            try {
                val queryParam = OverpassApi.getAllPubsQuery(userLocation)
                val response = OverpassApi.retrofitService.getResponse(queryParam)
                val transformedPubs = transformAllPubs(response.elements)
                nearPubDao.deleteForUser(credentials.uid!!)
                for (pub: NearPubDataModel in transformedPubs) {
                    nearPubDao.insert(pub)
                }
                saveCurrentPub(transformedPubs[0], userLocation)
            } catch (e: Exception) {
                Log.d("error add pubId", e.message.toString())
            }
            _isLoading.value = View.GONE
        }
    }

    private suspend fun saveCurrentPub(pub: NearPubDataModel, userLocation: UserLocationDataModel) {
        val pinnedPub = PinnedPub(
            id = pub.id,
            name = pub.name!!,
            type = pub.amenity!!,
            latitude = pub.lat,
            longitude = pub.lon
        )
        val userId = credentials.uid!!
        val bearerToken = "Bearer ${credentials.access}"
        PubApi.retrofitService.locateInPub(userId, bearerToken, pinnedPub)
        userLocation.pubId = pub.id
        userLocationDao.insert(userLocation)
    }

    private fun transformAllPubs(allPubsJson: Array<PubJson>): Array<NearPubDataModel> {
        var transformedPubs = arrayOf<NearPubDataModel>()
        for (pub: PubJson in allPubsJson) {
            if (pub.tags.name !== null && pub.tags.name !== "") {
                transformedPubs += pub.toDataModelWithDistance(currentLocation.value!!)
            }
        }
        transformedPubs.sortWith { first: NearPubDataModel, second: NearPubDataModel ->
            (first.distance?.minus(second.distance!!))?.toInt() ?: 0
        }
        return transformedPubs
    }


    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            application.applicationContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            application.applicationContext,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun setCurrentPub(pub: NearPubDataModel) {
        viewModelScope.launch {
            try {
                if (currentLocation.value !== null) {
                    saveCurrentPub(pub, currentLocation.value!!)
                }
            } catch (e: Exception) {
                Log.d("error set current pub:", e.message.toString())
            }
        }
    }

    fun getCurrentPubName(): String {
        if (allPubs.value !== null && currentLocation.value !== null && currentLocation.value!!.pubId !== null) {
            for (pub: NearPubDataModel in allPubs.value!!) {
                if (pub.id === currentLocation.value!!.pubId) {
                    return pub.name!!
                }
            }
        }
        return ""
    }
}

class SetPubViewModelFactory(
    private val fusedLocationClient: FusedLocationProviderClient,
    private val application: Application,
    private val userLocationDao: UserLocationDao,
    private val nearPubDao: NearPubDao,
    private val credentials: UserCredentials
    ) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SetPubViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SetPubViewModel(fusedLocationClient, application, userLocationDao, nearPubDao, credentials) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}